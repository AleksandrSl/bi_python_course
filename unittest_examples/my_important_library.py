import random


class GenomicInterval:

    def __init__(self, contig, start, end):
        self.contig = contig
        self.start = start
        self.end = end

    def __str__(self):
        return f'{self.contig}:{self.start}-{self.end}'

    def intersect(self, other):
        """
        Returns new GenomicInterval that represents intersection between self and other
        or None if intervals don't intersect each other
        """
        assert isinstance(other, GenomicInterval), f'You cannot intersect {other} with GenomicInterval'
        if self.contig == other.contig and self.start < other.end and self.end > other.start:
            return GenomicInterval(self.contig, max(self.start, other.start), min(self.end, other.end))
        else:
            return None


def random_dna_sequence_generator():
    while True:
        yield random.choice('ATGC')
