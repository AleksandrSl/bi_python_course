# Examples of unit tests

## Requirements

* python >= 3.6

## Usage

Tests can be run with:
```
python3.6 -m unittest
```

Take notice, that tests folder must be a python module (`__init__.py` must present)