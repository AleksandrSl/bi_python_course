import unittest

from my_important_library import GenomicInterval


class GenomicIntervalTests(unittest.TestCase):

    def test_intervals_intersection(self):
        '''
        1            |40-------------100|
        2    |15---------------70|
        3  |1-------------------------------500|
        4                                      |500---------1500|
        '''
        first_interval = GenomicInterval("chr1", 40, 100)
        second_interval = GenomicInterval("chr1", 15, 70)
        third_interval = GenomicInterval("chr1", 1, 500)
        fourth_interval = GenomicInterval("chr1", 500, 1500)

        first_second_intersection = first_interval.intersect(second_interval)
        self.assertEqual(first_second_intersection.contig, "chr1")
        self.assertEqual(first_second_intersection.start, 40)
        self.assertEqual(first_second_intersection.end, 70)

        first_third_intersection = first_interval.intersect(third_interval)
        self.assertEqual(first_third_intersection.contig, "chr1")
        self.assertEqual(first_third_intersection.start, 40)
        self.assertEqual(first_third_intersection.end, 100)

        self.assertIsNone(first_interval.intersect(fourth_interval))

        # We can write some messages that will better describe out test
        # Message will be print when this assertion is failed
        self.assertIsNone(third_interval.intersect(fourth_interval), 'second interval starts where first ends')

    def test_intersection_of_intervals_with_different_contigs(self):
        intersection = GenomicInterval("chr1", 40, 100).intersect(GenomicInterval("chr2", 15, 70))
        self.assertIsNone(intersection)

    def test_throws_on_intersection_with_other_objects(self):
        interval = GenomicInterval("chr1", 40, 100)

        def intersect_with_string():
            interval.intersect('Look I\'m a string')

        self.assertRaises(AssertionError, intersect_with_string)
        # You can't write just
        # self.assertRaises(AssertionError, interval.intersect('Look I\'m a string'))
        # This way interval.intersect('Look I\'m a string') will be executed before
        # unittest can do something and AssertionError will just be thrown

        # You can also use context manager form of self.assertRaises
        # This is just another form of the assertion above
        with self.assertRaises(AssertionError):
            # Only one error will be tested in one context manager
            GenomicInterval("chr1", 40, 100).intersect(666)

            raise ValueError(
                'This code will not be executed, if code above works as it\'s supposed and throws AssertionError')

        # So if you wanna test other situation where error is thrown, you need one more context manager
        #
        # This way unittest will save thrown error in variable e
        with self.assertRaises(AssertionError) as e:
            GenomicInterval("chr1", 40, 100).intersect({1: 8, 9: 1})

        # And you can for example test that exception contains text you wanna it to contain
        self.assertEqual(e.exception.args[0], 'You cannot intersect {1: 8, 9: 1} with GenomicInterval')

    def test_string_representation(self):
        interval = GenomicInterval("chr1", 40, 100)
        self.assertEqual(str(interval), 'chr1:40-100')
