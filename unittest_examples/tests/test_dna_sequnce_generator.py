import itertools
import unittest

from my_important_library import random_dna_sequence_generator


class DnaSequenceGeneratorTests(unittest.TestCase):

    def test_sequence_is_dna(self):
        # Takes some time but your computer is still working, generation of so
        # much bases at once would be fatality for RAM
        # islice is used to restrict generator for first 1000000000 elements
        for base in itertools.islice(random_dna_sequence_generator(), 1000000000):
            self.assertIn(base, 'ATGC')
